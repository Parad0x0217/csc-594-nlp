import os
import re
import sys
import enum
import operator
from collections import defaultdict
from nltk.tokenize import sent_tokenize

# August Karlstedt
# CSC 594 - Topics in AI: NLP


class ReplacementAction(enum.Enum):
    DEFAULT = 0
    CUSTOM = 1


class Main:
    REGEX_LEADING_PUNCTUATION = re.compile(r'^(\W+)', re.U)
    REGEX_TRAILING_PUNCTUATION = re.compile(r'(\W+)$', re.U)
    REGEX_NEWLINE = re.compile(r'[\r\n]{2,}', re.U|re.M)

    FILTERS = [
        ["n't", ReplacementAction.DEFAULT, "not"],
        ["'ll", ReplacementAction.DEFAULT, "will"],
        ["'ve", ReplacementAction.DEFAULT, "have"],
        ["'d",  ReplacementAction.DEFAULT, "would"],
        ["'re", ReplacementAction.DEFAULT, "are"],
        ["'s", ReplacementAction.CUSTOM, lambda word, position: [word[:position], "is"] if word[:position] in ["he", "she", "it"] else [word[:position], "'s"]],
        ["'m", ReplacementAction.DEFAULT, "am"]
    ]

    def __init__(self):
        if len(sys.argv) != 3:
            raise Exception('Invalid number of arguments. Format: main input.txt output.txt')

        input_file_path = sys.argv[1]
        output_file_path = sys.argv[2]

        if not os.path.exists(input_file_path):
            raise Exception('Input file does not exist')

        if not os.path.exists(os.path.dirname(os.path.abspath(output_file_path))):
            raise Exception('Output file directory does not exist')

        # everything's ok, let's open the files for read/write
        input_file = open(input_file_path, mode='r')
        output_file = open(output_file_path, mode='w')

        # read and tokenize the text into sentences
        text = input_file.read()
        input_file.close()

        sentences = sent_tokenize(text)
        types = defaultdict(int)

        word_count = 0
        for sentence in sentences:
            parts = self._split_sentence(sentence)
            word_count += len(parts)
            for item in parts:
                types[item] += 1

        types = sorted(types.items(), key=operator.itemgetter(0))
        types = sorted(types, key=operator.itemgetter(1), reverse=True)

        print('# of paragraphs = {}'.format(str(len(self.REGEX_NEWLINE.findall(text)) + 1)), file=output_file)
        print('# of sentences = {}'.format(len(sentences)), file=output_file)
        print('# of tokens = {}'.format(word_count), file=output_file)
        print('# of types = {}'.format(len(types)), file=output_file)
        print(file=output_file)
        print("================================", file=output_file)
        for type, count in types:
            print("{} {}".format(type, count), file=output_file)
        print(file=output_file)

        output_file.close()

    def _split_sentence(self, sentence):
        parts = []

        for word in str.split(sentence):
            parts.extend(self._split_word(word))

        return parts

    def _split_word(self, word):
        result = []

        leading_punctuation = self.REGEX_LEADING_PUNCTUATION.search(word)
        word = self.REGEX_LEADING_PUNCTUATION.sub("", word)
        trailing_punctuation = self.REGEX_TRAILING_PUNCTUATION.search(word)
        word = self.REGEX_TRAILING_PUNCTUATION.sub("", word)

        if leading_punctuation:
            result.extend(leading_punctuation.group())

        result.extend(self._apply_filters(word))

        if trailing_punctuation:
            result.extend(trailing_punctuation.group())

        return result

    def _apply_filters(self, word):
        if len(word) == 0:
            return []

        for filter in self.FILTERS:
            position = word.find(filter[0])
            if position < 0:
                continue

            if filter[1] == ReplacementAction.DEFAULT:
                return [word[:position], filter[2]]
            elif filter[1] == ReplacementAction.CUSTOM:
                return filter[2](word, position)

        return [word]