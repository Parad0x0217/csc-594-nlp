import os
import re
import sys
import enum
import math
import operator
from collections import defaultdict
import nltk.tokenize as nltk

# August Karlstedt
# CSC 594 - Topics in AI: NLP


class Main:

    SENTENCE_BEGIN_MARKER = '<s>'
    SENTENCE_END_MARKER = '</s>'

    def __init__(self):
        if len(sys.argv) != 4:
            raise Exception('Invalid number of arguments. Format: main.py train.txt test.txt output.txt')

        train_file_path = sys.argv[1]
        test_file_path = sys.argv[2]
        output_file_path = sys.argv[3]

        if not os.path.exists(train_file_path):
            raise Exception('Input file does not exist')

        if not os.path.exists(test_file_path):
            raise Exception('Test file does not exist')

        if not os.path.exists(os.path.dirname(os.path.abspath(output_file_path))):
            raise Exception('Output file directory does not exist')

        # everything's ok, let's open the files for read/write
        input_file = open(train_file_path, mode='r')
        test_file = open(test_file_path, mode='r')
        output_file = open(output_file_path, mode='w')

        # read and tokenize the text into sentences
        text = input_file.read()
        input_file.close()

        test_text = test_file.read()
        test_file.close()

        # train
        sentences = nltk.sent_tokenize(text)
        unigram_counts = defaultdict(int)
        bigram_counts = defaultdict(int)

        # token (word) count
        word_count = 0
        for sentence in sentences:
            parts = [self.SENTENCE_BEGIN_MARKER, *nltk.word_tokenize(sentence), self.SENTENCE_END_MARKER]
            part_length = len(parts)

            # subtract one from part_length so we don't include the BEGIN_SENTENCE_MARKER in word_count
            word_count += part_length - 1

            unigram_counts[parts[0]] += 1

            for i in range(1, part_length):
                word_first = parts[i - 1]
                word_second = parts[i]
                unigram_counts[word_second] += 1
                bigram_counts[(word_first, word_second)] += 1

        # calculate probabilities
        unigram_probabilities = defaultdict(float)
        for type, count in unigram_counts.items():
            unigram_probabilities[type] = math.log(count / word_count)

        bigram_probabilities = defaultdict(float)
        for (word_first, word_second), count in bigram_counts.items():
            bigram_probabilities[(word_first, word_second)] = math.log(count / unigram_counts[word_first])

        # test
        test_sentences = nltk.sent_tokenize(test_text)
        test_sentence_count = 1

        average_unigram_probability = 0.0
        average_bigram_probability = 0.0

        unigram_perplexity = 0.0
        bigram_perplexity = 0.0

        # word (token) count
        test_word_count = 0

        for sentence in test_sentences:
            print('Sentence {}: {}'.format(test_sentence_count, sentence), file=output_file)
            parts = [self.SENTENCE_BEGIN_MARKER, *nltk.word_tokenize(sentence), self.SENTENCE_END_MARKER]
            part_length = len(parts)

            # subtract one from part_length so we don't include the BEGIN_SENTENCE_MARKER in test_word_count
            test_word_count += part_length - 1

            unigram_probability = 0.0
            bigram_probability = 0.0

            for i in range(1, part_length):
                word_first = parts[i - 1]
                word_second = parts[i]
                unigram_probability += unigram_probabilities[word_second]
                bigram_probability += bigram_probabilities[(word_first, word_second)]

            unigram_probability = math.exp(unigram_probability)
            bigram_probability = math.exp(bigram_probability)

            average_unigram_probability += unigram_probability
            average_bigram_probability += bigram_probability

            unigram_perplexity += math.log(unigram_probability)
            bigram_perplexity += math.log(bigram_probability)

            print(' - unigram [Prob] {}'.format(unigram_probability), file=output_file)
            print(' - bigram  [Prob] {}'.format(bigram_probability), file=output_file)
            print(file=output_file)

            test_sentence_count += 1

        print('==========================', file=output_file)
        print('* Probability:', file=output_file)

        average_unigram_probability /= max(len(test_sentences), 1)
        average_bigram_probability /= max(len(test_sentences), 1)

        print('- Average unigram probability: {}'.format(average_unigram_probability), file=output_file)
        print('- Average bigram probability: {}'.format(average_bigram_probability), file=output_file)
        print(file=output_file)

        unigram_perplexity = math.exp(-1 / max(test_word_count, 1) * unigram_perplexity)
        bigram_perplexity = math.exp(-1 / max(test_word_count, 1) * bigram_perplexity)

        print('----------', file=output_file)
        print('* Perplexity:', file=output_file)
        print('- Unigram perplexity: {}'.format(unigram_perplexity), file=output_file)
        print('- Bigram perplexity: {}'.format(bigram_perplexity), file=output_file)

        output_file.close()
